// function* is the syntax used to create a generator function

// yield allows a generator function to pause its communication and pass control to an external system
// - the external system can push a value into the generator function body
// - the external system can throw an exception into the generator function body

// ---------------------------------------------------------------------------
// Lazy Generator
// --------------------------------------------------------------------------
function* infiniteSequence() {
    var i = 0;
    while(true) {
        yield i++;
    }
}

var infiniteIterator = infiniteSequence();
/*
while (true) {
    console.log(infiniteIterator.next()); // { value: xxxx, done: false } forever and ever
}
*/

// --------------------------------------------------------------------------

function* finiteSequence(){
    let index = 0;
    while(index < 3)
        yield index++;
}

let finiteIterator = finiteSequence();

console.log(finiteIterator.next()); // { value: 0, done: false }
console.log(finiteIterator.next()); // { value: 1, done: false }
console.log(finiteIterator.next()); // { value: 2, done: false }
console.log(finiteIterator.next()); // { done: true }

// --------------------------------------------------------------------------

function* generator(){
    console.log('Execution started');
    yield 0;
    console.log('Execution resumed');
    yield 1;
    console.log('Execution resumed');
}

var iterator = generator();
console.log('Starting iteration'); // This will execute before anything in the generator function body executes
console.log(iterator.next()); // { value: 0, done: false }
console.log(iterator.next()); // { value: 1, done: false }
console.log(iterator.next()); // { value: undefined, done: true }

// --------------------------------------------------------------------------

/*
    You can control the resulting value of the yield expression
    using iterator.next(valueToInject).
*/
function* otherGenerator() {
    const bar = yield 'foo'; // bar may be *any* type
    console.log(bar); // bar!
}

const otherIterator = otherGenerator();
// Start execution till we get first yield value
const otherfoo = otherIterator.next();
console.log(otherfoo.value); // foo
// Resume execution injecting bar
const othernext = otherIterator.next('bar');
// Since yield returns the parameter passed to the iterator's next function,
// and all iterators' next functions accept a parameter of any type,
// TypeScript will always assign the any type to the result of the yield operator (bar above).
console.log(othernext.value);

// --------------------------------------------------------------------------

/*
    You can throw an exception at the point of the yield expression
    using iterator.throw(error)
*/

function* throwGenerator() {
    try {
        yield 'foo';
    }
    catch(err) {
        console.log("Catch error!")
        console.log(err.message); // bar!
    }
}

var throwIterator = throwGenerator();
// Start execution till we get first yield value
var throwfoo = throwIterator.next();
console.log(throwfoo.value); // foo
// Resume execution throwing an exception 'bar'
var thrownext = throwIterator.throw(new Error('bar'));
