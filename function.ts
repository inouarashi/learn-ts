// ---------------------------------------------------------------------------
// Named function
// ---------------------------------------------------------------------------
function namedFunc(x, y) {
    return x + y;
}

// ---------------------------------------------------------------------------
// Typed function
// ---------------------------------------------------------------------------
function typedFunc(x: number, y: number): number {
    return x + y;
}

// ---------------------------------------------------------------------------
// Anonymous function
// ---------------------------------------------------------------------------
let anonymousFunc = function(x, y) { return x + y; };

// Parameters
function funcWithParams(firstName: string, lastName: string) {
    return firstName + " " + lastName;
}

// -> let result1 = funcWithParams("Bob");                  // error, too few parameters
// -> let result2 = funcWithParams("Bob", "Adams", "Sr.");  // error, too many parameters
let result3 = funcWithParams("Bob", "Adams");         // ah, just right

// ---------------------------------------------------------------------------
// Optional Parameters
// ---------------------------------------------------------------------------
function funcWithOptionalParams(firstName: string, lastName?: string) {
    if (lastName)
        return firstName + " " + lastName;
    else
        return firstName;
}

let result1 = funcWithOptionalParams("Bob");                  // works correctly now
// -> let result2 = funcWithOptionalParams("Bob", "Adams", "Sr.");  // error, too many parameters
result3 = funcWithOptionalParams("Bob", "Adams");         // ah, just right

// ---------------------------------------------------------------------------
// Default Parameters
// ---------------------------------------------------------------------------
function funcWithDefaultParams(firstName: string, lastName = "Smith") {
    return firstName + " " + lastName;
}

result1 = funcWithDefaultParams("Bob");                  // works correctly now, returns "Bob Smith"
let result2 = funcWithDefaultParams("Bob", undefined);       // still works, also returns "Bob Smith"
// -> result3 = funcWithDefaultParams("Bob", "Adams", "Sr.");  // error, too many parameters
let result4 = funcWithDefaultParams("Bob", "Adams");         // ah, just right


// Rest Parameters
function funcWithRestParams(firstName: string, ...restOfName: string[]) {
    return firstName + " " + restOfName.join(" ");
}

// employeeName will be "Joseph Samuel Lucas MacKinzie"
result1 = funcWithRestParams("Joseph", "Samuel", "Lucas", "MacKinzie");

// ---------------------------------------------------------------------------
// Overloading
// ---------------------------------------------------------------------------
let suits = ["hearts", "spades", "clubs", "diamonds"];

function pickCard(x: {suit: string; card: number; }[]): number;
function pickCard(x: number): {suit: string; card: number; };
function pickCard(x): any {
    // Check to see if we're working with an object/array
    // if so, they gave us the deck and we'll pick the card
    if (typeof x == "object") {
        let pickedCard = Math.floor(Math.random() * x.length);
        return pickedCard;
    }
    // Otherwise just let them pick the card
    else if (typeof x == "number") {
        let pickedSuit = Math.floor(x / 13);
        return { suit: suits[pickedSuit], card: x % 13 };
    }
}

let myDeck = [{ suit: "diamonds", card: 2 }, { suit: "spades", card: 10 }, { suit: "hearts", card: 4 }];
let pickedCard1 = myDeck[pickCard(myDeck)];
alert("card: " + pickedCard1.card + " of " + pickedCard1.suit);

let pickedCard2 = pickCard(15);
alert("card: " + pickedCard2.card + " of " + pickedCard2.suit);
