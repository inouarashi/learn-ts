
// ---------------------------------------------------------------------------
// Iterable
// ---------------------------------------------------------------------------

// for...of
let someArray = [1, "string", false];

for (let entry of someArray) {
    console.log(entry); // 1, "string", false
}

// for...in
let list = [4, 5, 6];

for (let i in list) {
    console.log(i); // "0", "1", "2",
}

for (let i of list) {
    console.log(i); // "4", "5", "6"
}


// ---------------------------------------------------------------------------
// Iterator
// ---------------------------------------------------------------------------

class MyIterator {

    step: number;
    constructor() {
        this.step = 0;
    }
    [Symbol.iterator]() {
        return this;
    }
    next() {
        this.step++;

        if (this.step === 1)
            return {value: 'Ben'};
        else if (this.step == 2)
            return {value: 'Ilegbodu'};

        return {done: true};
    }
}

let iter = new MyIterator();

// output: {value: 'Ben'}
console.log(iter.next());
// output: {value: 'Ilegbodu'}
console.log(iter.next());
// output: {done: true}
console.log(iter.next());
// output: {done: true}
console.log(iter.next());

