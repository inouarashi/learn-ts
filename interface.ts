// ---------------------------------------------------------------------------
// Interface
// ---------------------------------------------------------------------------

interface Name {
    first: string;
    second: string;
}

// Declaration
var interfaceNom: Name;
interfaceNom = {
    first: 'John',
    second: 'Doe'
};

// Inline type annotation
var interfaceName: {
    first: string;
    second: string;
};
interfaceName = {
    first: 'John',
    second: 'Doe'
};

// ERROR
/*
interfaceName = {           // Error : `second` is missing
    first: 'John'
};
interfaceName = {           // Error : `second` is the wrong type
    first: 'John',
    second: 1337
};
*/
