// ---------------------------------------------------------------------------
// Promise
// ---------------------------------------------------------------------------

const resolvePromise = new Promise((resolve, reject) => {
    resolve(123);
});

resolvePromise.then((res) => {
    console.log('I get called:', res === 123); // I get called: true
});

resolvePromise.catch((err) => {
    // This is never called
});

// ---------------------------------------------------------------------------
// Chain-ability of Promises
// ---------------------------------------------------------------------------

setTimeout(() =>
Promise.resolve(123)
    .then((res) => {
        console.log(res); // 123
        return 456;
    })
    .then((res) => {
        console.log(res); // 456
        return Promise.resolve(123); // Notice that we are returning a Promise
    })
    .then((res) => {
        console.log(res); // 123 : Notice that this `then` is called with the resolved value
        return 123;
    })
, 1000);

// ---------------------------------------------------------------------------

setTimeout(() =>
Promise.reject(new Error('something bad happened'))
    .then((res) => {
        console.log(res); // not called
        return 456;
    })
    .then((res) => {
        console.log(res); // not called
        return 123;
    })
    .then((res) => {
        console.log(res); // not called
        return 123;
    })
    .catch((err) => {
        console.log(err.message); // something bad happened
    })
, 2000);

// ---------------------------------------------------------------------------

// The catch returns a new promise (effectively creating a new promise chain)
setTimeout(() =>
Promise.reject(new Error('something bad happened'))
    .then((res) => {
        console.log(res); // not called
        return 456;
    })
    .catch((err) => {
        console.log(err.message); // something bad happened
        return 123;
    })
    .then((res) => {
        console.log(res); // 123
    })
, 3000);

// ---------------------------------------------------------------------------

// Any synchronous errors thrown in a then (or catch)
// result in the returned promise to fail
setTimeout(() =>
Promise.resolve(123)
    .then((res) => {
        throw new Error('something bad happened'); // throw a synchronous error
        return 456;
    })
    .then((res) => {
        console.log(res); // never called
        return Promise.resolve(789);
    })
    .catch((err) => {
        console.log(err.message); // something bad happened
    })
, 4000);

// ---------------------------------------------------------------------------

// Only the relevant (nearest tailing) catch
// is called for a given error (as the catch starts a new promise chain).
setTimeout(() =>
Promise.resolve(123)
    .then((res) => {
        throw new Error('something bad happened'); // throw a synchronous error
        return 456;
    })
    .catch((err) => {
        console.log('first catch: ' + err.message); // something bad happened
        return 123;
    })
    .then((res) => {
        console.log(res); // 123
        return Promise.resolve(789);
    })
    .catch((err) => {
        console.log('second catch: ' + err.message); // never called
    })
, 5000);
