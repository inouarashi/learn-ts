# Learn TypeScript

> Learn Typescript with simple examples

## Overview

- Typed superset of JavaScript that compiles to plain JavaScript.
- Runs on any platform that JavaScript runs on.
- Object Oriented Language
- Static type-checking:
- DOM Manipulation:
- ES 6 Features (includes most features of planned ECMAScript 2015 (ES 6, 7))

## Installation

Dependencies:

- [Node](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/)

Run the following coommand:

```
npm install -g typescript
```

## Setup

### Transpilation

A TypeScript code is written in a file with `.ts` extension. You need to compile into javascript (Transpilation).

### Configuration

The presence of a tsconfig.json file in a directory indicates that the directory is the root of a TypeScript project. The tsconfig.json file specifies the root files and the compiler options required to compile the project. A project is compiled in one of the following ways:

Using tsconfig.json:

- By invoking tsc with no input files, in which case the compiler searches for the tsconfig.json file starting in the current directory and continuing up the parent directory chain.
- By invoking tsc with no input files and a --project (or just -p) command line option that specifies the path of a directory containing a tsconfig.json file, or a path to a valid .json file containing the configurations.

When input files are specified on the command line, tsconfig.json files are ignored

### Project

The following command will create a sample tsconfig.json (with few options set):

```
tsc --init
```

### Compilation

Compile (with tsconfig.json):

```
tsc
```

Compile (Without tsconfig.json):

```
tsc file.ts
```

### Run

Once compiled, you can run the corresponding `js` file.

```
node file.js
```

## Playground

- [types](type.ts)
- [interfaces](interface.ts)
- [functions](function.ts)
- [classes](classe.ts)
- [advanced-types](advanced-type.ts)
- [iterators](iterator.ts)
- [generators](generator.ts)
- [promises](promise.ts)