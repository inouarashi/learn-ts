// ----------------------------------------------------------------------------
// Alias
// ----------------------------------------------------------------------------

type StrOrNum = string|number;
var simpleAlias: StrOrNum;
simpleAlias = 123;
simpleAlias = '123';

interface IStudent {
    id: string;
    age: number;
}

interface IWorker {
    companyId: string;
}

type IStudentAlias = IStudent;
type ICustomType = IStudent | IWorker;

let enumAlias: IStudentAlias = {
    id: 'ID3241',
    age: 2
};

// ----------------------------------------------------------------------------
// Union
// ----------------------------------------------------------------------------

function formatCommandline(command: string[] | string) {
    var line = '';
    if (typeof command === 'string') {
        line = command.trim();
    } else {
        line = command.join(' ').trim();
    }
    console.log(line);
    // Do stuff with line: string
}

formatCommandline("je suis une fougere");
formatCommandline(["je", "suis", "une", "mouche"]);

// Intersection
function intersectionFunc<T, U>(first: T, second: U): T & U {
    return { ...first, ...second };
}

const x = intersectionFunc({ a: "hello" }, { b: 42 });

// x now has both `a` and `b`
const a = x.a;
const b = x.b;

// ----------------------------------------------------------------------------
// Guards
// ----------------------------------------------------------------------------

// instanceof
class Foo {
    foo = 123;
    common = '123';
}

class Bar {
    bar = 123;
    common = '123';
}

function doStuff(arg: Foo | Bar) {
    if (arg instanceof Foo) {
        console.log(arg.foo); // OK
        // -> console.log(arg.bar); // Error!
    }
    if (arg instanceof Bar) {
        // -> console.log(arg.foo); // Error!
        console.log(arg.bar); // OK
    }

    console.log(arg.common); // OK
    // -> console.log(arg.foo); // Error!
    // -> console.log(arg.bar); // Error!
}

doStuff(new Foo());
doStuff(new Bar());

// in
interface A {
    x: number;
}
interface B {
    y: string;
}

function doOtherStuff(q: A | B) {
    if ('x' in q) {
      // q: A
    }
    else {
      // q: B
    }
}
