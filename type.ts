// ----------------------------------------------------------------------------
// Boolean
// ----------------------------------------------------------------------------

let isDone: boolean = false;
let isNotDone: boolean = true;
// -> isDone = 1234 // Error, it's a number


// ----------------------------------------------------------------------------
// Number
// ----------------------------------------------------------------------------

let numDecimal: number = 6;
let numHex: number = 0xf00d;
let numBinary: number = 0b1010;
let numOctal: number = 0o744;


// ---------------------------------------------------------------------------
// String
// ---------------------------------------------------------------------------

let strColor: string = "blue";
strColor = 'red';

// Template
let strFullName: string = `Bob Bobbington`;
let strAge: number = 37;
let strSentence1: string = `Hello, my name is ${ strFullName }. I'll be ${ strAge + 1 } years old next month.`;
let strSentence2: string = "Hello, my name is " + strFullName + ".\n\n" + "I'll be " + (strAge + 1) + " years old next month.";


// ---------------------------------------------------------------------------
// Array
// ---------------------------------------------------------------------------

let arrayNumberlist: number[] = [1, 2, 3];
let arrayNumberGeneric: Array<number> = [1, 2, 3];


// ---------------------------------------------------------------------------
// Tuple
// ---------------------------------------------------------------------------

// Declare a tuple type
let tupleX: [string, number];

// Initialize it
tupleX = ["hello", 10]; // OK

// Initialize it incorrectly
// -> tupleX = [10, "hello"]; // Error

// accessing
console.log(tupleX[0].substring(1)); // OK
// -> console.log(tupleX[1].substring(1)); // Error, 'number' does not have 'substring'
// -> tupleX[3] = "world"; // Error, Property '3' does not exist on type '[string, number]'.
// -> console.log(tupleX[5].toString()); // Error, Property '5' does not exist on type '[string, number]'.


// ---------------------------------------------------------------------------
// Enum
// ---------------------------------------------------------------------------

enum Color {Red, Green, Blue}
let c: Color = Color.Green;

enum otherColor {Red = 1, Green, Blue}
let oc: otherColor = otherColor.Green;

enum anotherColor {Red = 1, Green = 2, Blue = 4}
let ac: anotherColor = anotherColor.Green;

enum Colour {Red = 1, Green, Blue}
let colourName: string = Colour[2];

console.log(colourName); // Displays 'Green' as its value is 2 above


// ---------------------------------------------------------------------------
// Any
// ---------------------------------------------------------------------------

let anySure: any = 4;
anySure = "maybe a string instead";
anySure = false; // okay, definitely a boolean

let notSure: any = 4;
// notSure.ifItExists(); // okay, ifItExists might exist at runtime
notSure.toFixed(); // okay, toFixed exists (but the compiler doesn't check)

let prettySure: Object = 4;
// -> prettySure.toFixed(); // Error: Property 'toFixed' doesn't exist on type 'Object'.

let anyList: any[] = [1, true, "free"];
anyList[1] = 100;


// ---------------------------------------------------------------------------
// Void
// ---------------------------------------------------------------------------

function warnUser(): void {
    console.log("This is my warning message");
}

// Not really useful
let unusable: void = undefined;


// ---------------------------------------------------------------------------
// Null and Undefined
// ---------------------------------------------------------------------------

// Not much else we can assign to these variables!
let u: undefined = undefined;
let n: null = null;


// ---------------------------------------------------------------------------
// Never
// ---------------------------------------------------------------------------

// Function returning never must have unreachable end point
function error(message: string): never {
    throw new Error(message);
}

// Inferred return type is never
function fail() {
    return error("Something failed");
}

// Function returning never must have unreachable end point
function infiniteLoop(): never {
    while (true) {
    }
}


// ---------------------------------------------------------------------------
// Object
// ---------------------------------------------------------------------------

function create(o: object | null): void {}

create({ prop: 0 }); // OK
create(null); // OK

// -> create(42); // Error
// -> create("string"); // Error
// -> create(false); // Error
// -> create(undefined); // Error


// ---------------------------------------------------------------------------
// Assertions
// ---------------------------------------------------------------------------

let someValue: any = "this is a string";
let strLength1: number = (<string>someValue).length;
let strLength2: number = (someValue as string).length;
